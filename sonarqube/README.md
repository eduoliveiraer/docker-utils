### Para rodar o sonarqube em um projeto, é necessário rodar o seguinte comando:
   ```
   docker run \
   --rm \
   --network=host \
   -e SONAR_HOST_URL="http://127.0.0.1:9000" \
   -e SONAR_SCANNER_OPTS="-Dsonar.projectKey=api-ad" \
   -e SONAR_TOKEN="sqp_7c9ed69e4cf74a289847d4941a1878c86b8a1ddd" \
   -v "/home/eduardo/projetos/sipomweb/api-ad:/usr/src" \
   sonarsource/sonar-scanner-cli
